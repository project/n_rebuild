
if (Drupal.jsEnabled) {
  $(document).ready(function() {

    $('.ff_clear_link').each(function() {
        var el = $(this);
        el.mousedown(function(){return false;});
    });

    $('.form-item .description').hover(
        function(){$(this).addClass('description-hover');},
        function(){$(this).removeClass('description-hover');}
    );
    
    if (nx_ie_version == 6) {
        $('.tabs_pri li').hover(
            function(){$(this).addClass('hover');},
            function(){$(this).removeClass('hover');}
        );
    }

 /* --- [EN] PNG Fix (for IE6) ------------------------------------------------------------- */

    if (nx_ie_version == 6 && nx_png_fix_mode) {
        $(document).find("img[@src$=.png]").each(function() {
            var png_img = $(this),
                old_src = png_img.attr('src');
            png_img.css('width', png_img.width() + 'px');
            png_img.css('height', png_img.height() + 'px');
            png_img.attr('src', nx_empty_gif_path);
            png_img.get(0).runtimeStyle.filter = 'progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\'' + old_src + '\', sizingMethod=\'scale\');';
        });
    }

 /* --- [EN] Hints ------------------------------------------------------------------------- */

    set_hint('#search-block-form #edit-search-block-form-1');
    set_hint('#user-login-form #edit-name');
    set_hint('#user-login-form #edit-pass');

 /* --- [EN] Example of cookie usage -------------------------------------------------------
    $.cookie('nx_param1', 'value1', {expires: 1000, path: '/'}); // for setting cookie value
    var nx_param1 = $.cookie('nx_param1');                       // for getting cookie value
    ---------------------------------------------------------------------------------------- */

  });
}

 /* --- [EN] FUNCTIONS --------------------------------------------------------------------- */

    function set_hint(element_path){
        try {
            var element = $(element_path);
            element.focus(nx_hint_show);
            element.blur(nx_hint_hide);
            nx_hint_hide.call(element);
        } catch(e){}
    }

    function nx_hint_show(){
        if ($(this).attr('value') == '') {
            $(this).removeClass($(this).attr('id')+'-Hint');
        }
    }

    function nx_hint_hide(){
        if ($(this).attr('value') == '') {
            $(this).addClass($(this).attr('id')+'-Hint');
        }
    }

