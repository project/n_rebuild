<div class="c_comment comment<?php
    print ' '. $zebra;
    print ' comment-'. $zebra;
    print ($comment->new ? ' comment-new' : '');
    print ' comment-'. $status;
    print ' comment_depth-'. $comment->depth;
?>">
<div class="c_comment-subStyle">

    <h3 class="c_comment_title"><?php print $title ?></h3>

    <?php if ($submitted) { ?>
        <span class="c_comment-submitted submitted">
            <?php print t('Published') .' '; ?>
            <?php print '<span class="username">' . t('!username', array('!username' => theme('username', $comment))) .'</span> '; ?>
            <?php print '<span class="datetime">' . t('!datetime', array('!datetime' => format_date($comment->timestamp, 'custom', 'D, d/m/Y - H:i'))) . '</span>'; ?>
        </span>
    <?php } ?>

    <?php if (strlen($picture) > 50) { ?>
        <div class="c_user_picture">
            <?php print $picture ?>
        </div>
    <?php } ?>

    <?php if ($comment->new) { ?>
        <span class="new"><?php print drupal_ucfirst($new) ?></span>
    <?php } ?>

    <div class="c_comment_content content">
        <?php print $content ?>
        <?php if ($signature) { ?>
            <div class="signature">
                <?php print $signature ?>
            </div>
        <?php } ?>
    </div>
    
    <div class="c_clear">&nbsp;</div>

    <?php if ($links) { ?>
        <div class="c_comment_links c_links"><?php print $links ?></div>
    <?php } ?>

</div>
</div>