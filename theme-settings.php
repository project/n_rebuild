<?php


    function phptemplate_settings($saved_settings) {
        $defaults = array(
            'nx_meta_keywords' => 'keywords...',
            'nx_meta_description' => 'description...',
            'nx_png_fix_mode' => FALSE,
        );
        $defaults = array_merge($defaults, theme_get_settings());
        $settings = array_merge($defaults, $saved_settings);

        $form['nx_container']['seo'] = array(
            '#type' => 'fieldset',
            '#title' => t('Search engine optimization (SEO) settings'),
            '#collapsible' => TRUE,
            '#collapsed' => FALSE,
        );
        $form['nx_container']['seo']['meta']['nx_meta_keywords'] = array(
            '#type' => 'textfield',
            '#title' => t('Meta keywords'),
            '#description' => t('Enter a comma-separated list of keywords'),
            '#size' => 60,
            '#default_value' => $settings['nx_meta_keywords'],
        );
        $form['nx_container']['seo']['meta']['nx_meta_description'] = array(
            '#type' => 'textarea',
            '#title' => t('Meta description'),
            '#cols' => 60,
            '#rows' => 6,
            '#default_value' => $settings['nx_meta_description'],
        );

        $form['nx_container']['png_fix'] = array(
            '#type' => 'fieldset',
            '#title' => t('PNG Fix'),
            '#collapsible' => TRUE,
            '#collapsed' => FALSE,
        );
        $form['nx_container']['png_fix']['nx_png_fix_mode'] = array(
            '#type' => 'checkbox',
            '#title' => t('Enable PNG Fix'),
            '#default_value' => $settings['nx_png_fix_mode'],
            '#description' => t('Fixing the transparent PNG images in IE6 (not work with backgrounds).'),
        );

        return $form;
    }

