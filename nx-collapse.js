

Drupal.behaviors.collapse = function (context) {
  $('fieldset.collapsible > legend:not(.collapse-processed)', context).each(function() {

    var fieldset = $(this).parent(),
        title = $('.c_fieldset_title:first', fieldset),
        title_text_acc = $('.c3', title),
        title_text = title_text_acc.text();

 // [EN] Expand if there are errors inside
    if ($('input.error, textarea.error, select.error', fieldset).size() > 0) {
        fieldset.removeClass('collapsed');
    }

    title_text_acc.empty();
    title_text_acc.append($('<a href="#"></a>'));
    var title_as_link = $('a:first', title);
    title_as_link.html(title_text);

    title_as_link.mousedown(function(){return false;});
    title_as_link.click(function(){
        if (!fieldset.is('.collapsed')) {fieldset.addClass('collapsed');}
        else {fieldset.removeClass('collapsed');}
        return false;
    });

    $(this).addClass('collapse-processed');

  });
};
