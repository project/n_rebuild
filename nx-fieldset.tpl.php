<fieldset <?php print $attributes; ?>><legend></legend>
<div class="fieldset-subStyle">

    <?php if ($title) { ?>
        <h2 class="c_fieldset_title title" title="<?php print $title; ?>"><span class="c1 cx"><span class="c2 cx"><span class="c3 cx">
            <?php print truncate_utf8($title, 40, FALSE, TRUE); ?>
        </span></span></span></h2>
    <?php } ?>

    <div class="collapsible-area">
        <div class="fieldset-content_cover-1"><div class="fieldset-content_cover-2">
            <?php if ($description) {print '<div class="fieldset-description">'. $description .'</div>';} ?>
            <div class="c_clear">&nbsp;</div>
            <div class="fieldset-content">
                <?php if ($children) {print $children;} ?>
                <?php if ($value) {print $value;} ?>
            </div>
            <div class="c_clear">&nbsp;</div>
        </div></div>
    </div>

</div>
</fieldset>