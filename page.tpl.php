<?php print '<?xml version="1.0"?>'; ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
<head>
<?php print $head; ?>
<title><?php print $head_title; ?></title>
<?php print $styles; ?>
<?php print $scripts; ?>
</head>

<body class="<?php print $nx_body_classes; ?>">


    <table class="page struct">
        <tr>
            <td class="L_Field struct">&nbsp;</td>
            <td class="M_Field struct">


                <table class="before_head_part page_part struct">
                    <tr>
                        <td class="column_1 struct">
                            <div class="region_description">before head &gt; column 1</div>
                            <!-- COLUMN 1 -->
                                <div class="column_1-cover-1">
                                <div class="column_1-cover-2">
                                    <?php print $before_head_column_1_region; ?>
                                </div>
                                </div>
                            <!-- END COLUMN 1 -->
                            <img class="min_size" src="<?php print $full_theme_path; ?>images/empty.gif" alt="" />
                        </td>

                        <td class="column_2 struct">
                            <div class="region_description">before head &gt; column 2</div>
                            <!-- COLUMN 2 -->
                                <div class="column_2-cover-1">
                                <div class="column_2-cover-2">
                                    <?php print $before_head_column_2_region; ?>
                                </div>
                                </div>
                            <!-- END COLUMN 2 -->
                            <img class="min_size" src="<?php print $full_theme_path; ?>images/empty.gif" alt="" />
                        </td>

                        <td class="column_3 struct">
                            <div class="region_description">before head &gt; column 3</div>
                            <!-- COLUMN 3 -->
                                <div class="column_3-cover-1">
                                <div class="column_3-cover-2">
                                    <?php print $before_head_column_3_region; ?>
                                </div>
                                </div>
                            <!-- END COLUMN 3 -->
                            <img class="min_size" src="<?php print $full_theme_path; ?>images/empty.gif" alt="" />
                        </td>
                    </tr>
                </table>


                <table class="head_part page_part struct">
                    <tr>
                        <td class="column_1 struct">
                            <div class="region_description">logo [not region]</div>
                            <!-- COLUMN 1 -->
                                <div class="column_1-cover-1">
                                <div class="column_1-cover-2">
                                    <a class="logo ff_clear_link" href="<?php print check_url($front_page); ?>" title="<?php print $site_title; ?>">
                                        <img src="<?php print check_url($logo); ?>" alt="<?php print $site_title; ?>" />
                                    </a><div class="c_clear"></div>
                                </div>
                                </div>
                            <!-- END COLUMN 1 -->
                            <img class="min_size" src="<?php print $full_theme_path; ?>images/empty.gif" alt="" />
                        </td>

                        <td class="column_2 struct">
                            <div class="region_description">head &gt; column 2</div>
                            <!-- COLUMN 2 -->
                                <div class="column_2-cover-1">
                                <div class="column_2-cover-2">
                                    <?php print $header; ?>
                                </div>
                                </div>
                            <!-- END COLUMN 2 -->
                            <img class="min_size" src="<?php print $full_theme_path; ?>images/empty.gif" alt="" />
                        </td>

                        <td class="column_3 struct">
                            <div class="region_description">head &gt; column 3</div>
                            <!-- COLUMN 3 -->
                                <div class="column_3-cover-1">
                                <div class="column_3-cover-2">
                                    <?php print $head_column_3_region; ?>
                                </div>
                                </div>
                            <!-- END COLUMN 3 -->
                            <img class="min_size" src="<?php print $full_theme_path; ?>images/empty.gif" alt="" />
                        </td>
                    </tr>
                </table>


                <table class="body_part page_part struct">
                    <tr>
                        <td class="column_1 struct">
                            <div class="region_description">body &gt; column 1</div>
                            <!-- COLUMN 1 -->
                                <div class="column_1-cover-1">
                                <div class="column_1-cover-2">
                                    <?php if ($left) { ?>
                                        <?php print $left; ?>
                                    <?php } ?>
                                </div>
                                </div>
                            <!-- END COLUMN 1 -->
                            <img class="min_size" src="<?php print $full_theme_path; ?>images/empty.gif" alt="" />
                        </td>

                        <td class="column_2 struct">
                            <div class="region_description">body &gt; column 2</div>
                            <!-- COLUMN 2 -->
                                <div class="column_2-cover-1">
                                <div class="column_2-cover-2">
                                    <?php print $breadcrumb; ?>
                                    <?php if ($title) {print '<h1 class="c_page_title">'. $title .'</h1><div class="c_clear">&nbsp;</div>';} ?>
                                    <?php if ($tabs_pri || $tabs_sec) { ?>
                                        <div class="all_tabs">
                                            <?php print $tabs_pri; ?>
                                                <div class="tabs_bounder"></div>
                                            <?php print $tabs_sec; ?>
                                        </div>
                                    <?php } ?>
                                    <?php if ($show_messages && $messages) {print $messages;} ?>
                                    <?php print $help; ?>
                                    <?php print $content; ?>
                                </div>
                                </div>
                            <!-- END COLUMN 2 -->
                            <img class="min_size" src="<?php print $full_theme_path; ?>images/empty.gif" alt="" />
                        </td>

                        <td class="column_3 struct <?php print !$right ? 'body_part-column_3-invisible' : ''; ?>">
                            <div class="region_description">body &gt; column 3</div>
                            <!-- COLUMN 3 -->
                                <div class="column_3-cover-1">
                                <div class="column_3-cover-2">
                                    <?php if ($right) { ?>
                                        <?php print $right; ?>
                                    <?php } ?>
                                </div>
                                </div>
                            <!-- END COLUMN 3 -->
                            <img class="min_size" src="<?php print $full_theme_path; ?>images/empty.gif" alt="" />
                        </td>
                    </tr>
                </table>


                <table class="foot_part page_part struct">
                    <tr>
                        <td class="column_1 struct">
                            <div class="region_description">foot &gt; column 1</div>
                            <!-- COLUMN 1 -->
                                <div class="column_1-cover-1">
                                <div class="column_1-cover-2">
                                    <?php print $foot_column_1_region; ?>
                                </div>
                                </div>
                            <!-- END COLUMN 1 -->
                            <img class="min_size" src="<?php print $full_theme_path; ?>images/empty.gif" alt="" />
                        </td>

                        <td class="column_2 struct">
                            <div class="region_description">foot &gt; column 2</div>
                            <!-- COLUMN 2 -->
                                <div class="column_2-cover-1">
                                <div class="column_2-cover-2">
                                    <?php print $footer; ?>
                                    <?php if (strlen($footer_message) > 1) { ?>
                                        <div class="footer_message">
                                            <?php print $footer_message; ?>
                                        </div>
                                    <?php } ?>
                                </div>
                                </div>
                            <!-- END COLUMN 2 -->
                            <img class="min_size" src="<?php print $full_theme_path; ?>images/empty.gif" alt="" />
                        </td>

                        <td class="column_3 struct">
                            <div class="region_description">foot &gt; column 3</div>
                            <!-- COLUMN 3 -->
                                <div class="column_3-cover-1">
                                <div class="column_3-cover-2">
                                    <?php print $foot_column_3_region; ?>
                                </div>
                                </div>
                            <!-- END COLUMN 3 -->
                            <img class="min_size" src="<?php print $full_theme_path; ?>images/empty.gif" alt="" />
                        </td>
                    </tr>
                </table>


            </td>
            <td class="R_Field struct">&nbsp;</td>
        </tr>
    </table>



<div class="techno_bottom_region">
    <?php print $techno_bottom_region; ?>
</div>

<?php global $user; if ($user->uid == 0) { ?>
    <div class="preLoad">
        <div style="background:url(<?php print $full_theme_path; ?>images/draggable-active.png)">&nbsp;</div>
    </div>
<?php } ?>

<?php /* if ($mission) {print '<div id="mission">'. $mission .'</div>';} */ ?>
<?php /* if ($site_slogan) {print '<div id="site_slogan">'. $site_slogan .'</div>';} */ ?>
<?php /* if (isset($primary_links)) {print theme('links', $primary_links, array('class' => 'primary_links'), array('outer_prefix' => '', 'inner_prefix' => '<span><span>', 'inner_suffix' => '</span></span>', 'outer_suffix' => ''));} */ ?>
<?php /* if (isset($secondary_links)) {print theme('links', $secondary_links, array('class' => 'secondary_links'), array('outer_prefix' => '', 'inner_prefix' => '<span><span>', 'inner_suffix' => '</span></span>', 'outer_suffix' => ''));} */ ?>
<?php print $closure; ?>
</body>
</html>
