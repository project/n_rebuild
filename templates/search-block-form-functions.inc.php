<?php



    function phptemplate_preprocess_search_block_form(&$vars, $hook) {

        $vars['form']['search_block_form']['#title'] = t('');
        $vars['form']['submit']['#value'] = t('');
        $vars['form']['submit']['#attributes'] = array('onmousedown' => 'return!1');

        foreach (element_children($vars['form']) as $key) {
            if (isset($vars['form'][$key]['#printed']) && $vars['form'][$key]['#printed'] == 1) {
                unset($vars['form'][$key]['#printed']);
            }
        }

        $vars['rendered_search_form']['keys'] = drupal_render($vars['form']['search_block_form']);
        $vars['rendered_search_form']['submit'] = drupal_render($vars['form']['submit']);
        $vars['rendered_search_form']['form'] = drupal_render($vars['form']);

    }
