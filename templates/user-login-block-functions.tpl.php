<?php



    global $nx_theme_registration_pool;
           $nx_theme_registration_pool['user_login_block'] = array(
                'template' => 'templates/user-login-block',
                'arguments' => array('form' => NULL), 
                'preprocess functions' => array('phptemplate_preprocess_user_login_block_form'),
           );


    function phptemplate_preprocess_user_login_block_form(&$vars, $hook) {

        $vars['form']['name']['#title'] = t('Username');
        $vars['form']['pass']['#title'] = t('Password');
        $vars['form']['submit']['#value'] = t('Enter');
        unset($vars['form']['links']);

        $vars['rendered_login_form']['name'] = drupal_render($vars['form']['name']);
        $vars['rendered_login_form']['pass'] = drupal_render($vars['form']['pass']);
        $vars['rendered_login_form']['submit'] = drupal_render($vars['form']['submit']);
        $vars['rendered_login_form']['reg_link'] = variable_get('user_register', 1) ? l(t('Create new account'), 'user/register', array('attributes' => array('title' => t('Create a new user account.')))) : '';
        $vars['rendered_login_form']['rmd_link'] = l(t('Request new password'), 'user/password', array('attributes' => array('title' => t('Request new password via e-mail.'))));

        $vars['rendered_login_form']['form'] = drupal_render($vars['form']);

    }

